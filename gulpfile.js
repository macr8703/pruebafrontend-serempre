var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var image = require('gulp-image');
var watch = require('gulp-watch');
gulp.task('sass-cmarp', function(){
	return gulp.src('./assets/sass/style.sass')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(autoprefixer({
			browsers: ['last 10 versions'],
			cascade: false
		}))
		.pipe(sourcemaps.write('./', {
			includeContent: false,
			sourceRoot: './assets/sass/style.sass'
		}))
		.pipe(gulp.dest('./web/css/'))
		.pipe(browserSync.stream({match: '**/*.css'}));
});
gulp.task('uglify', function(){
	return gulp.src('./assets/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./web/js/'));
});
gulp.task('image', function () {
	gulp.src('./assets/image/*')
	  .pipe(image())
	  .pipe(gulp.dest('./web/image/'));
});
gulp.task('watch',function(){
	gulp.watch('./assets/sass/*.sass', gulp.series('sass-cmarp'));
	gulp.watch('./assets/js/*.js', gulp.series('uglify'));
	gulp.watch('./web/js/*.js').on('change', browserSync.reload);
	gulp.watch('**/*.html').on('change', browserSync.reload);
});
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});
gulp.task('default', gulp.parallel('sass-cmarp', 'uglify', 'image','watch','browser-sync'));