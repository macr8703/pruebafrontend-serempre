function counterNumbres(){
	$('.counter').each(function () {
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, {
			duration: 4000,
			easing: 'swing',
			step: function (now) {
				$(this).text(Math.ceil(now));
			}
		});
	});
}
function activateCounterOnScroll(){
	var flag = 1;
	$(window).scroll(function() {
		var hT = $('footer').offset().top,
			hH = $('footer').outerHeight(),
			wH = $(window).height(),
			wS = $(this).scrollTop();
		if (wS > (hT+hH-wH)){
			console.log(flag);
			if(flag == 1){
				counterNumbres();
				flag++;
			}
			
		}
	 });
}
function mainBanner(){
	var gh = $(window).height();
	var gw = $(window).width();
	$(".main-banner").height(gh);
	$(".main-banner").width(gw);
}
function hoverMaskWH(){
	$(".photo-content > img").each(function(){
		var getw = $(this).width();
		var geth = $(this).height();
		$(this).parent().find(".text-icon").width(getw);
		$(this).parent().find(".text-icon").height(geth);
	});
}
$(document).ready(function(){
	mainBanner();
	hoverMaskWH();
	activateCounterOnScroll();
	$(".search-box").on("click",function(){
		$(".search-box-content").slideToggle();
	});
	$(".clouse-menu").on("click",function(){
		$(".menu.mobile").fadeOut();
	});
	$(".hamburger").on("click",function(){
		$(".menu.mobile").fadeIn();
	});
});
$(window).on("resize",function(){
	mainBanner();
	hoverMaskWH();
});